package com.kubernetes.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class Controller {

    @GetMapping
    public ResponseEntity<String> getCustomerName(){
        return ResponseEntity.of(Optional.of("Hello World"));
    }
}
