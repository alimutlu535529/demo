FROM maven:3.6.0-jdk-11-slim as build
RUN mkdir -p /project/source
COPY . project/source
WORKDIR project/source
RUN mvn clean install

FROM openjdk:8-jre-alpine
WORKDIR /app
COPY --from=build project/source/target/demo-*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
